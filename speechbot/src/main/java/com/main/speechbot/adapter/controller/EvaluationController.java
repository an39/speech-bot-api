package com.main.speechbot.adapter.controller;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.main.speechbot.adapter.dto.api.ApiResponse;
import com.main.speechbot.adapter.dto.api.EvaluationInputDTO;
import com.main.speechbot.usecases.EvaluationService;

import kong.unirest.ObjectMapper;

@RestController
@RequestMapping("/evaluate")
public class EvaluationController {

	@Autowired
	EvaluationService evaluationService;
	
	@PostMapping
	public ApiResponse evaluateRecording(@RequestParam String text,
            @RequestParam("user_audio_file") MultipartFile file)
	{
		return evaluationService.evaluateRecording(text, file);
	}
}
