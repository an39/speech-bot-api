package com.main.speechbot.adapter.dto.api;

public class ApiResponse {
    private String status;
    private Object value;
    public static ApiResponse success(Object value){
        return new ApiResponse(StatusPayload.SUCCESS,value);
    }

    public static ApiResponse failed(Object value){
        return new ApiResponse(StatusPayload.FAILED,value);
    }
    public static ApiResponse error(Object value){
        return new ApiResponse(StatusPayload.ERROR,value);
    }
    public ApiResponse(String status, Object value) {
        this.status = status;
        this.value = value;
    }

    public ApiResponse(String status){
        this.status = status;
    }

    public ApiResponse() {
    }




    public String getStatus() {
        return this.status;
    }

    public Object getValue() {
        return this.value;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof ApiResponse)) return false;
        final ApiResponse other = (ApiResponse) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$status = this.getStatus();
        final Object other$status = other.getStatus();
        if (this$status == null ? other$status != null : !this$status.equals(other$status)) return false;
        final Object this$value = this.getValue();
        final Object other$value = other.getValue();
        if (this$value == null ? other$value != null : !this$value.equals(other$value)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof ApiResponse;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $status = this.getStatus();
        result = result * PRIME + ($status == null ? 43 : $status.hashCode());
        final Object $value = this.getValue();
        result = result * PRIME + ($value == null ? 43 : $value.hashCode());
        return result;
    }

    public String toString() {
        return "UniversalPayload(status=" + this.getStatus() + ", value=" + this.getValue() + ")";
    }
}

