package com.main.speechbot.adapter.dto.api;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class EvaluationInputDTO {
	String text;
}
