package com.main.speechbot.adapter.dto.api;

public class StatusPayload {
    public static final String SUCCESS = "success";
    public static final String FAILED = "failed";
    public static final String ERROR = "error";

    String status;

    public StatusPayload(String status){
        this.status =status;
    }

    public StatusPayload() {
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof StatusPayload)) return false;
        final StatusPayload other = (StatusPayload) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$status = this.getStatus();
        final Object other$status = other.getStatus();
        if (this$status == null ? other$status != null : !this$status.equals(other$status)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof StatusPayload;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $status = this.getStatus();
        result = result * PRIME + ($status == null ? 43 : $status.hashCode());
        return result;
    }

    public String toString() {
        return "StatusPayload(status=" + this.getStatus() + ")";
    }
}
