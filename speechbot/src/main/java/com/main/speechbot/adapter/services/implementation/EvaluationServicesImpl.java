package com.main.speechbot.adapter.services.implementation;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.main.speechbot.adapter.dto.api.ApiResponse;
import com.main.speechbot.adapter.util.MultipartInputStreamFileResource;
import com.main.speechbot.usecases.EvaluationService;

import kong.unirest.json.JSONObject;

@Service
public class EvaluationServicesImpl implements EvaluationService {

	public ApiResponse evaluateRecording(String text, MultipartFile file)
	{
		String url = "https://api.speechace.co/api/scoring/text/v0.5/json?key=5DY%2FTK%2B6faTZHc0uRMIuQ%2Fmp%2Ba7l1hr%2FcpnopcA4G4wj%2Fy8p3KR9iVhRsnJyMViBnOplqlBZIKPkRiO17hHX%2BfxMEyc6ODImjsk8noGi9KF5Uy9I6UUSdTfApNNWdmzF&dialect=en-us";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		MultiValueMap<String, Object> body
		  = new LinkedMultiValueMap<>();
		body.add("text", text);
		try {
			body.add("user_audio_file", new MultipartInputStreamFileResource(file.getInputStream(), file.getOriginalFilename()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HttpEntity<MultiValueMap<String, Object>> requestEntity
		 = new HttpEntity<>(body, headers);
		RestTemplate restTemplate = new RestTemplate();
		JsonNode response = restTemplate
		  .postForObject(url, requestEntity, JsonNode.class);
		return ApiResponse.success(response);
				

			    

	}

	
}
