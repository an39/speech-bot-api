package com.main.speechbot.usecases;

import org.springframework.web.multipart.MultipartFile;

import com.main.speechbot.adapter.dto.api.ApiResponse;

public interface EvaluationService {
	ApiResponse evaluateRecording(String text, MultipartFile file);
}
